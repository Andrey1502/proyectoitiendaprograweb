<?php
require ('header.php');
error_reporting(0);
session_start();
if ($_SESSION && $_SESSION['usuario']){
  if($_SESSION && $_SESSION['privilegio'] !=0){
     header("Location: ../Administrador/main.php");

  }
  
}
else{
    header("Location: ../login/login.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Catálogo de productos</title>
    <link rel="stylesheet" href="styleHeader.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css" />

</head>
<body>

<form action= "#" method= "POST">

<div>
<label>Seleccionar una categoría:</label>
<?php

//Obtener categorias del sistema
 include "../conexion.php";
 $result = mysqli_query($enlace,"SELECT * FROM categorias");
 
?>

<select onchange="submit()" name="cates">
<option selected disabled>--Sin Seleccionar--</option>
<?php

//Obtener categorias del sistema
 include "../conexion.php";
 while($fila=mysqli_fetch_array($result)){
    
     $nombreCategoria= $fila['nombre'];
     echo "<option>$nombreCategoria</option>";
 }
?>
    
</select>


</div>

</form>

<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
<thead>
<tr>
    
    
    <th>Nombre</th>
    <th>Descripción</th>
    <th>Imagen</th>
    
    <th>Precio</th>
    <th>Acciones</th>

</tr>

</thead>

<tbody>
<?php
 
   
include('../conexion.php');

$categoria = $_POST['cates'];
    
        
$result = mysqli_query($enlace,"SELECT * FROM categorias WHERE nombre = '".$categoria."'");
$datosCategoria= mysqli_fetch_array($result);
        
$idCategoria = $datosCategoria['id'];


$resultado = mysqli_query($enlace,"SELECT * FROM productos WHERE categoria = '".$idCategoria."'");

      
    
    
    
  while($filas=mysqli_fetch_array($resultado)){
  
  ?>

  <tr>
    <td><?php echo $filas['nombre'] ?> </td>
    <td><?php echo $filas['descripcion'] ?> </td>
    <td><?php echo '<img src = "'.$filas["imagen"].'" width="100" height="100"></img>' ?> </td>
    
    <td><?php echo $filas['precio'] ?> </td>
    <td><a href="annadirCarrito.php?id=<?php echo $filas['id'] ?>" >Añadir al carrito</td>
</tr>

</tbody>
<?php
  }
    

?>
</table>


<a href="main.php">Volver a la página principal</a>

</body>
</html>



<?php
require ('header.php');
error_reporting(0);
session_start();
if ($_SESSION && $_SESSION['usuario']){
  if($_SESSION && $_SESSION['privilegio'] !=0){
     header("Location: ../Administrador/main.php");

  }
  
}
else{
    header("Location: ../login/login.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ver Detalle de compra</title>
    <link rel="stylesheet" href="styleHeader.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css" />
</head>
<body>
<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
<thead>
<tr>
    
    
    <th>Producto</th>
    <th>Descripción</th>
    <th>Imagen</th>
    <th>Precio</th>
    <th>Cantidad Solicitada</th>

  

    

</tr>

</thead>

<tbody>
<?php
 
include('../conexion.php');

$idCliente= $_SESSION['id'];
    
$idCompra = $_GET['id'];


$result = mysqli_query($enlace,"SELECT * FROM productos_compra WHERE id_compra = '".$idCompra."'");


$contador=mysqli_num_rows($result);


$i=1;
while($i <= $contador) {
  $resu= mysqli_fetch_array($result);
  $idProducto = $resu['id_producto'];
  $productos= mysqli_query($enlace,"SELECT * FROM productos WHERE id = '".$idProducto."'");

  $datosProducto = mysqli_fetch_array($productos);


?>

  

  <tr>
  
    <td><?php echo $datosProducto['nombre']?> </td>
    <td><?php echo $datosProducto['descripcion']?> </td>
    <td><?php echo '<img src = "'.$datosProducto['imagen'].'" width="100" height="100"></img>' ?> </td>
    <td><?php echo $resu['monto_producto']?> </td>
    <td><?php echo $resu['cantidad_requerida']?> </td>
   
   
</tr>

</tbody>
<?php
  $i++;
   }
?>

</table>
<a href="verCompras.php">Volver a la página anterior</a>
</body>
</html>
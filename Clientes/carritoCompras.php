<?php
require ('header.php');
error_reporting(0);
session_start();
if ($_SESSION && $_SESSION['usuario']){
  if($_SESSION && $_SESSION['privilegio'] !=0){
     header("Location: ../Administrador/main.php");

  }
  
}
else{
    header("Location: ../login/login.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Carrito de Compras</title>
    <link rel="stylesheet" href="styleHeader.css">
    <link rel="stylesheet" href="styleCarro.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css" />
</head>
<body class="cuerpoApp">

<form action= "#" method="post">
<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
<thead>
<tr>
    
    
    <th>Nombre</th>
    <th>Descripción</th>
    <th>Imagen</th>
    <th>Precio</th>
    <th>Cantidad Requerida</th>
    <th>Acciones</th>
    <th class="idProducto">Id</th>

</tr>

</thead>

<tbody>
<?php
 
include('../conexion.php');

$idCliente= $_SESSION['id'];
    
$result = mysqli_query($enlace,"SELECT * FROM carrito_clientes WHERE id_cliente = '".$idCliente."'");

$contador=mysqli_num_rows($result);


$i=1;
while($i <= $contador) {

  $datosCompras= mysqli_fetch_array($result);

  $cantidad= $datosCompras['cantidad_requerida'];

   $idProducto= $datosCompras['id_producto'];

   $resultado = mysqli_query($enlace,"SELECT * FROM productos WHERE id = '".$idProducto."'");

   $filas = mysqli_fetch_array($resultado);
?>

  

  <tr>
  
    <td><?php echo $filas['nombre']?> </td>
    <td><?php echo $filas['descripcion']?> </td>
    <td><?php echo '<img src = "'.$filas['imagen'].'" width="100" height="100"></img>' ?> </td>
    <td><?php echo $filas['precio']?> </td>
    
    <td><input type="number" min="1" name="cantidadProducto[]" value= <?php echo $cantidad?> ></td>
    <td><a href="eliminarCompra.php?id=<?php echo $filas['id'] ?>" >Descartar</td>
    
    <td>
    <input type="hidden" name="idProductos[]" value= <?php echo $filas['id']?>>
  </td>
   
</tr>

</tbody>
<?php
  $i++;
   }
?>

</table>

<div class="contenedor">

<div>
<button name="guardarCambios">Guardar Cambios en la cantidad requerida</button>
</div>

<div>
<button name="comprar">Comprar Productos</button>
</div>

</div>

<a href="main.php">Volver a la página principal</a>
</form>

</body>
</html>


<?php
if(isset($_POST['guardarCambios'])){

  include('../conexion.php');
  
  $idCliente= $_SESSION['id'];
  
  $result = mysqli_query($enlace,"SELECT * FROM carrito_clientes WHERE id_cliente = '".$idCliente."'");

  $contador=mysqli_num_rows($result);

 

$i=0;
while($i < $contador) {
   $idProductos=$_POST['idProductos'];
 
   $idProductoRequerido = $idProductos[$i];


  

   $cantidadProductos= $_POST['cantidadProducto'];

   $cantidadRequerida = $cantidadProductos[$i];

  
    
   

  
    $sql = "UPDATE carrito_clientes SET cantidad_requerida = '".$cantidadRequerida."' WHERE id_producto = '".$idProductoRequerido."' AND id_cliente = '".$idCliente."'";


    $resultado = mysqli_query($enlace,$sql);

    $i++;
   
   
    header("Refresh:0");


}

}

?>


<?php
if(isset($_POST['comprar'])){

  include('../conexion.php');
  
  $idCliente= $_SESSION['id'];

  date_default_timezone_set("America/Costa_Rica");
    
  $fecha= date("Y-m-d h:i");

  $insertarDatos = "INSERT INTO compras(fecha,id_cliente) 
  VALUES('".$fecha."','".$idCliente."')";

  mysqli_query($enlace,$insertarDatos);
  
  $idCompra = mysqli_insert_id($enlace);

  echo"ID DE LA COMPRA: ".$idCompra;



  $result = mysqli_query($enlace,"SELECT * FROM carrito_clientes WHERE id_cliente = '".$idCliente."'");

  


  $contador=mysqli_num_rows($result);
  
  $i=0;

  
    while($i < $contador) {

      $resultadoConsulta= mysqli_fetch_array($result);
 
 
       $idProducto= $resultadoConsulta['id_producto'];
 
        
        $cantidadRequerida = $resultadoConsulta['cantidad_requerida'];
        
        $productos =  mysqli_query($enlace,"SELECT * FROM productos WHERE id = '".$idProducto."'"); 
 
        $datosProductos= mysqli_fetch_array($productos);
 
        $precio = $datosProductos['precio'];
 
 
        $insertarDatos = "INSERT INTO productos_compra(id_compra,id_producto,monto_producto,cantidad_requerida) 
        VALUES('".$idCompra."','".$idProducto."','".$precio."','".$cantidadRequerida."')";
 
 


   
         $resulta= mysqli_query($enlace,$insertarDatos);


         $actualizarRestante= "UPDATE productos SET restante= restante - $cantidadRequerida WHERE id = $idProducto";

         mysqli_query($enlace,$actualizarRestante);
         
         
          

         
         $i++;
 
         }

         $sql = "SELECT SUM(monto_producto * cantidad_Requerida) AS monto_total FROM productos_compra where id_compra = $idCompra";


         $total = mysqli_query($enlace,$sql);

         $data= mysqli_fetch_array($total);

  
         $totalCompra = $data['monto_total'];

        
        
         $actualizarTotal= "UPDATE compras SET total= '".$totalCompra."'  WHERE id = $idCompra";

         mysqli_query($enlace,$actualizarTotal);
        //Borrar registros del cliente en el carrito
        mysqli_query($enlace,"DELETE FROM carrito_clientes WHERE id_cliente = '".$idCliente."'");
  
    
         header("Refresh:0");
          
}
 


  

 





?>



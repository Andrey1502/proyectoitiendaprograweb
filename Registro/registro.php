<?php
error_reporting(0);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrarse al sistema</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css" />
    
    <link rel="stylesheet" href="styleRegistro.css" >

</head>
<body class="cuerpoApp">
    <!--Sección encabezado de la página-->
    <section class="hero is-small is-info is-bold">
        <div class="hero-body">
            <div class="container">
                <h1 class="encabezadoPrincipal">Sistema de Tienda Online</h1>

                <h1 class="seccionActual">Registrar una cuenta</h1>
            </div>
        </div>
    </section>

    <!--Sección para pedir los datos requeridos del usuario-->
    <section class="section">
        <div class="container">
            <div class="">
            <div class="">
                <form action="#" method="post">
                    <div class="datos form-group">
            
                        <div class="form-group">
                            <input class="input is-primary is-rounded" id="nombreCompletoUsuario" placeholder="Nombre" type="text" name="nombre" required>
                        </div>

                        <div class="form-group">
                            <input class="input is-primary is-rounded" placeholder="Primer Apellido" type="text" name="primerApellido" required>
                        </div>

                        <div class="form-group">
                            <input class="input is-primary is-rounded" placeholder="Segundo Apellido" type="text" name="segundoApellido" required>
                        </div>

                        <div class="form-group">
                            <input class="input is-primary is-rounded" placeholder="Teléfono" type="text" name="telefono" required>
                        </div>


                        <div class="form-group">
                            <input class="input is-primary is-rounded" id="correoUsuario" type="email" name="correo" placeholder="Correo" required>
                        </div>

                        <div class="form-group">
                            <input class="input is-primary is-rounded" placeholder="Direccion" type="text" name="direccion" required>
                        </div>

                       <div class="form-group">
                            <input class="input is-primary is-rounded" id="usuario" placeholder="Nombre de Usuario" type="text" name="nombreUsuario" required>
                        </div>
                    
                        <div class="form-group">
                            <input class="input is-primary is-rounded" id="contrasenna" placeholder="Contraseña" type="password" name="contrasenna" required>
                        </div>
                            
                      
                        <div>
                            
                            <button id="botonEnviar" type="submit" class="Registrar button is-success is-normal">Registrar</button>
                    
                        </div>
            
                </form>

            </div>
                
            </div> 
               

        </div>

            
      

    </section>
    
    
        
    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column"> 
                    <a href="../login/login.php">¿Ya tienes una cuenta?,<strong>Inicia sesión!!</strong> </a>
                </div>
              </div>
        </div>
      </section>
  


       

    
    
    
</body>
</html>


<?php

include '../conexion.php';
//Declarar variables
$nombre=$_POST['nombre'];
$primer_apellido=$_POST['primerApellido'];
$segundo_apellido=$_POST['segundoApellido'];
$telefono=$_POST['telefono'];
$correo=$_POST['correo'];
$direccion=$_POST['direccion'];
$nombre_usuario=$_POST['nombreUsuario'];
$contrasenna=$_POST['contrasenna'];
   
if($nombre!=null && $primer_apellido=!null && $segundo_apellido!=null && $telefono && $correo!=null && $direccion !=null
       && $nombre_usuario!=null && $contrasenna !=null){


$result = mysqli_query($enlace,"SELECT * FROM usuarios WHERE nombre_usuario = '".$nombre_usuario."'");
$contador= mysqli_num_rows($result);

if($contador > 0){
    echo '<script type="text/javascript">alert("Este nombre de usuario ya se encuentra en uso")</script>';

}

else if($contador==0){
    
    //Insertar en la base de datos
   
   $insertarDatos = "INSERT INTO usuarios(nombre,primer_apellido,segundo_apellido,telefono,correo,direccion,contrasenna,privilegio,nombre_usuario) 
   VALUES('".$nombre."','".$primer_apellido."','".$segundo_apellido."','".$telefono."','".$correo."','".$direccion."','".$contrasenna."','0','".$nombre_usuario."')";

  $result= mysqli_query($enlace,$insertarDatos);

   if($result){
    echo '<script type="text/javascript">alert("Sus datos se han registrado exitosamente")</script>';
    header("location:../login/login.php");
   }

   else{
    echo '<script type="text/javascript">alert("Fallo al registrar, favor revisar los datos e intentar nuevamente")</script>';

   }

       }
    }
   
?>
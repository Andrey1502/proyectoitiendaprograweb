<?php
require ('header.php');
error_reporting(0);
session_start();
if ($_SESSION && $_SESSION['usuario']){
  if($_SESSION && $_SESSION['privilegio'] !=1){
     header("Location: ../Clientes/main.php");

  }
  
}
else{
    header("Location: ../login/login.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Eliminar Productos</title>
    <link rel="stylesheet" href="estiloH.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css" />
</head>
<body>
<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
<thead>
<tr>
    
    <th>Nombre</th>
    <th>Descripción</th>
    <th>Imagen</th>
    <th>Categoría</th>
    <th>Restante</th>
    <th>Precio</th>
    <th>Acciones</th>

</tr>

</thead>

<tbody>
  <?php
  include('../conexion.php');

  $sql = "SELECT * FROM productos";
  $resultado = mysqli_query($enlace,$sql);
  while($filas=mysqli_fetch_array($resultado)){

  ?>

  <tr>
    
    <td><?php echo $filas['nombre'] ?></td>
    <td><?php echo $filas['descripcion'] ?></td>
    <td><?php echo '<img src = "'.$filas["imagen"].'" width="100" height="100"></img>' ?></td>

    <td>
    <?php 
        include('../conexion.php');
        
        $idCategoria = $filas['categoria'];
        $result = mysqli_query($enlace,"SELECT nombre FROM categorias WHERE id = '".$idCategoria."'"); 
        $datos= mysqli_fetch_array($result);
        
        
        
        
      echo $datos['nombre']?>
      
    </td>

    <td><?php echo $filas['restante'] ?></td>
    <td><?php echo $filas['precio'] ?></td>
    <td><a href="eliminarProducto.php?id=<?php echo $filas['id'] ?>" >Eliminar</td>
    
    

  </tr>

</tbody>
<?php
  }
?>
</table>
</table>
<a href="main.php">Volver a la página principal del administrador</a>

</body>
</html>


<?php
 $id = $_GET['id'];
 include('../conexion.php');
 $sql="DELETE FROM productos WHERE id='".$id."'";
 $resultado = mysqli_query($enlace,$sql);
 $contador= mysqli_num_rows($resultado);
 if($contador > 0){
    header("Refresh:0");
  
  
  }


?>
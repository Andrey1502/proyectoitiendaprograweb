<?php
require ('header.php');
error_reporting(0);
session_start();
if ($_SESSION && $_SESSION['usuario']){
  if($_SESSION && $_SESSION['privilegio'] !=1){
     header("Location: ../Clientes/main.php");

  }
  
}
else{
    header("Location: ../login/login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Eliminar Categoría</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css" />

    <link rel="stylesheet" href="estiloH.css">

</head>
<body>
<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
<thead>
<tr>
    
    <th>Nombre</th>
    <th>Acciones</th>

</tr>

</thead>

<tbody>
  <?php
    include('../conexion.php');

  $sql = "SELECT * from categorias";
  $resultado = mysqli_query($enlace,$sql);
  while($filas=mysqli_fetch_array($resultado)){

  ?>

  <tr>
    
    <td><?php echo $filas['nombre'] ?></td>
 
    <td><a href="eliminarCategoria.php?id=<?php echo $filas['id'] ?>" >Eliminar</a></td>
    

  </tr>

</tbody>
<?php
  }
?>
</table>
<a href="main.php">Volver a la página principal del administrador</a>

</body>
</html>

<?php

    $id = $_GET['id'];
    include('../conexion.php');
    $consulta= mysqli_query($enlace,"SELECT nombre FROM productos WHERE categoria = '".$id."'");
    $contador = mysqli_num_rows($consulta);
    if($contador == 0){
      $sql="DELETE FROM categorias WHERE id='".$id."'";
       $resultado = mysqli_query($enlace,$sql);

       $conta= mysqli_num_rows($resultado);

       if($conta>0){
        header("Refresh:0");

       }


    }
    else if($contador > 0){
      echo '<script type="text/javascript">alert("Esta categoría se encuentra asociada a un producto,por lo que no se puede eliminar")</script>';
    }
    

    
 
?>

<?php
error_reporting(0);
session_start();
if ($_SESSION && $_SESSION['usuario']){
  if($_SESSION && $_SESSION['privilegio'] !=1){
     header("Location: ../Clientes/main.php");

  }
  
}
else{
    header("Location: ../login/login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página de administradores</title>
    <link rel="stylesheet" href="estiloM.css">
    <link rel="stylesheet" href="estiloH.css">


</head>

<body class="cuerpoApp">

<?php require ('header.php') ?>

 
<div class="estadisticas">

    <?php

    //Obtener clientes registrados en el sistema
     include "../conexion.php";
     $result = mysqli_query($enlace,"SELECT * FROM usuarios WHERE privilegio != 1");
     $contador= mysqli_num_rows($result);

    //Obtener cantidad de productos vendidos

    $sql = "SELECT SUM(cantidad_Requerida) AS cantidad_total FROM productos_compra";


$total = mysqli_query($enlace,$sql);

$data= mysqli_fetch_array($total);

  
$totalProductosVendidos = $data['cantidad_total'];



//Obtener monto total de las ventas

$sql = "SELECT SUM(total) AS monto_total FROM compras";


$total = mysqli_query($enlace,$sql);

$data= mysqli_fetch_array($total);

  
$montoTotalProductosVendidos = $data['monto_total'];


    ?>
   <div class="clientes">
   <label>Clientes registrados:</label>
   
    <label><?php echo $contador?></label>
    </div>

    <div class="productosVendidos">
    <label>Cantidad de productos vendidos:</label>
    <label><?php echo $totalProductosVendidos?></label>

    </div>

    <div class="totalVentas">
    <label>Monto total de ventas:</label>
    <label><?php echo $montoTotalProductosVendidos?></label>

    </div>
    


   
</div>


</body>
</html>


<?php
error_reporting(0);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio se sesión</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css" />
    <link rel="stylesheet" href="styleLogin.css">

</head>

<body class="cuerpoApp">
    <!--Sección encabezado de la página-->
    <section class="hero is-small is-info is-bold">
        <div class="hero-body">
            <div class="container">
                <h1 class="encabezadoPrincipal column">Sistema de Tiendas Online</h1>

                <h1 class="seccionActual column">Inicio de sesión</h1>
            </div>
        </div>
    </section>


    <!--Sección para pedir los datos requeridos del usuario-->



    <section class="">
        <div class="container">
            <div class="">
                <div class="">
                    <div class="form-group">

                        <form  method="POST" action="#">
                            <div class="form-group">
                                <input type="text" class="input is-primary is-rounded" id="nombreUsuario" type="text"
                                    placeholder="Usuario" name="nUsu" required>
                            </div>

                            <div class="form-group">
                                <input class="input is-primary is-rounded" id="contrasenna" type="password"
                                    placeholder="Contraseña" name="passUsu" required>
                            </div>


                            <div class="form-group">
                                <button type="submit" name="iniciar" class="button is-success is-normal" class="iniciarSesion">Iniciar
                                    Sesión</button>
                            </div>


                        </form>
                    </div>
                </div>




            </div>


        </div>

    </section>


    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column">
                    <a type="rel" href="../Registro/registro.php">¿No tienes una cuenta?,<strong>Regístrate
                            ahora!!</strong></a>
                </div>
            </div>
        </div>
    </section>







</body>

</html>

<?php
if(isset($_POST['iniciar'])){
    
 //Declarar variables
 $usuario =$_POST["nUsu"];
 $contrasenna=$_POST["passUsu"];

 include "verificarLogin.php";
 verificar($usuario,$contrasenna);
 

}
?>